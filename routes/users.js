var express = require('express');
var router = express.Router();
const User = require('../models/usermodel.js')
const jwt = require('jsonwebtoken');


/* GET users listing. */
router.get('/', function(req, res, next) {

  User.findAll().then(user => {
		if (user) {
			return res.json({ respuesta: true, data: user , msj:"hola"})
		} else {
			return res.json({ respuesta: false, error: "Error al obtener los datos" })
		}

	})
});


/* POST Create users. */

router.post('/create', function(req, res, next) {

  if (!("name" in req.body) || !("password" in req.body)  || !("type" in req.body) ||  !("phone" in req.body) || !("email" in req.body)){
		return res.json({ respuesta: false, error: "Faltan datos" })
	}

  const token = jwt.sign({isid:req.body.email}, 'apitest_private_key' )
  console.log(token)

	User.create({name:req.body.name, token:token, pass:req.body.password, email:req.body.email, type:req.body.type, phone: req.body.phone}).then(user => {

		if (user) {
			return res.json({ respuesta: true, msj: "Guardado con exito!" })
		} else {
			return res.json({ respuesta: false, error: "Error al guardar datos" })
		}

	})

});

/* PUT Update users by id. */
router.put('/update/:id', function(req, res, next) {

  if (!("name" in req.body) || !("phone" in req.body) || !("email" in req.body)){
		return res.json({ respuesta: false, error: "Faltan datos" })
	}

  User.findOne({ where: {id:req.params.id} }).then(user => {
      if (user) {
        user.update({
          name: req.body.name,
          phone: req.body.phone,
          email: req.body.email,
        })
        .then(userupdate => {
          if(userupdate){
            return res.json({ respuesta: true, msj:"Registro actualizado correctamente.", data: userupdate })
          }else{
            return res.json({ respuesta: false, error: "Error al actualizar, intente mas tarde." })
          }
        })
      
      }else{
        return res.json({ respuesta: false, error: "No se ha encontrado el usuario." })
      }
  })

});


/* GET get users by id. */
router.get('/:id', function(req, res) {

  User.findOne({ where: {id:req.params.id} }).then(user => {
		if (user) {
			return res.json({ respuesta: true, data: user })
		} else {
			return res.json({ respuesta: false, error: "Error al obtener los datos." })
		}

	})

});

module.exports = router;
